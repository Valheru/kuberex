# specify the node base image with your desired version node:<version>
FROM node:8
WORKDIR /usr/src/app
COPY . .
RUN npm install
CMD npm start
# replace this with your application's default port
EXPOSE 5000